# bro

Project Opener

![Video showcasing user selecting a project from a menu which launches the project in the elementary OS Code program.](assets/bro-editor-opening.gif)

## Building, Testing, and Installation

You'll need the following dependencies to build:
* meson
* valac
* ncurses
* glib-2.0
* gobject-2.0
* gee-0.8
* gio-2.8

Run `meson build` to configure the build environment and run `ninja` to build
```Bash
meson build --prefix=/usr
cd build
ninja
```
To install, use `ninja install`, then execute with `bro`
```Bash
sudo ninja install
bro
```

## Configuration

It's a nightmare trying to set up smart defaults for this so by default, there are opinionated defaults.

However, you can customise the program's behaviour through the use of environment variables.

Note: The program appends the selected directory to editor and terminal launch commands so if you needs to use flags to launch a program in a certain directory, ensure that the flag is set last.

`BRO_EDITOR` - The command use to launch a code/text editor set to the selected directory. This has a higher priority over `BRO_TERMINAL`.

`BRO_TERMINAL` - The command used to launch a terminal program set to the selected directory.

`BRO_PROJECTS_PATH` - The parent directory that contains all the project directories.

