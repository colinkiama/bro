using Curses;
using Posix;
using Bro.Helpers;
using Bro.Model;
using Bro.Structs;
using Gee;

public class Demo {
    public MainLoop loop;
    private IOChannel io_channel;
    private ProgramConfig config;
    internal IOSource io;

    public string[] directory_names;
    public TreeMap<string, string> directory_path_map;
    public string selected_directory = "";

    public Demo (ProgramConfig config) {
        this.config = config;
        loop = new MainLoop ();
        io_channel = new IOChannel.unix_new (Posix.STDIN_FILENO);
        io = new IOSource (io_channel, IOCondition.IN);
        io.attach (loop.get_context ());
    }

    public void start () {
        Intl.setlocale (); // setlocale(LC_ALL, "")
        initscr ();
        curs_set (0); // hide cursor
        noecho ();
        cbreak ();
        stdscr.keypad (true);
        load_directory_path_data ();
    }

    public void load_directory_path_data () {
        Result<ArrayList<File>> directory_listing_result = FileHelper.list_directories_in_directory (
            File.new_for_path (config.projects_path)
        );


        if (directory_listing_result.has_error ()) {
            error (directory_listing_result.expose_error ().message);
        }

        var directories_found = directory_listing_result.reveal ();

        // var dir_path_iterator = directories_found.map<string> (f => f.get_path ()).filter (p => p != null);

        var dir_path_iterator = directories_found.map<DirectoryMapPair?> (
            f => DirectoryMapPair () { name = f.get_basename (), path = f.get_path () }
        ).filter (
            p => p.path != null
        );

        directory_path_map = new TreeMap<string, string> ();
        var names_to_return = new ArrayList<string> ();

        while (dir_path_iterator.has_next ()) {
            dir_path_iterator.next ();
            DirectoryMapPair dir_map_pair = dir_path_iterator.get ();
            names_to_return.add (dir_map_pair.name);
            directory_path_map.set (dir_map_pair.name, dir_map_pair.path);
        }

        names_to_return.sort ();
        directory_names = names_to_return.to_array ();

        // var dir_name_iterator = directories_found.map<string> (f => f.get_basename ());
        // var names_to_return = new ArrayList<string> ();
        // names_to_return.add_all_iterator (dir_name_iterator);
        // names_to_return.sort ();
        // string[] names_array = names_to_return.to_array ();
        // directory_path_map = create_directory_path_map (names_array, directories_found.to_array ());
        // return names_array;
    }

    public TreeMap<string, string> create_directory_path_map (string[] names, string[] paths) {
        var map_to_return = new TreeMap<string, string> ();

        for (int i = 0; i < names.length; i++) {
            map_to_return.set ((owned) names[i], (owned) paths[i]);
        }

        return map_to_return;
    }

    public void stop () {
        menu.unpost ();
        endwin ();
    }

    // private string[] choices = {
    //     "Choice 1",
    //     "Choice 2",
    //     "Choice 3",
    //     "Choice 4",
    //     "Choice
    //     "Choice 6",
    //     "Choice 7",
    //     "Exit",
    // };
    private Curses.Menu menu;
    private Window window;
    private Curses.MenuItem[] menu_items = {};

    [CCode (has_target = false)]
    private delegate void MenuItemCallback (string name);

    public void activate () {
        Curses.MenuItem item;
        MenuItemCallback cb = ((name) => {
            stdscr.move (20, 0);
            stdscr.clrtoeol ();
            stdscr.mvprintw (20, 0, "Item selected is : %s", name);
        });

        foreach (unowned string name in directory_names) {
            item = new Curses.MenuItem (name, "");
            item.set_userptr ((void *)cb);
            menu_items += (owned) item;
        }

        menu = new Curses.Menu (menu_items);
        stdscr.mvprintw (LINES - 3, 0, "Press <ENTER> to see the option selected");
        stdscr.mvprintw (LINES - 2, 0, "Up and Down arrow keys to naviage (F1 to Exit)");

        menu.post ();
        stdscr.noutrefresh ();
        window.noutrefresh ();

        io.set_callback (() => {
            var c = getch ();

            switch (c) {
                case Key.F0 + 1:
                    loop.quit ();
                    return Source.REMOVE;
                case Key.DOWN: menu.driver (MenuRequest.DOWN_ITEM); break;
                case Key.UP: menu.driver (MenuRequest.UP_ITEM); break;
                case 10: // Enter
                    unowned Curses.MenuItem current_itm = menu.current_item;
                    MenuItemCallback p;

                    p = (MenuItemCallback) current_itm.userptr;
                    p (current_itm.name);
                    selected_directory = directory_path_map.get (current_itm.name);

                    menu.pos_cursor ();
                    loop.quit ();
                    return Source.REMOVE;
                default:
                    break;
            }

            stdscr.noutrefresh ();
            window.noutrefresh ();
            redraw ();

            return Source.CONTINUE;
        });
    }

    public void run () {
        loop.run ();
    }

    public void redraw () {
        doupdate ();
    }

    static int main (string[] args) {
        ProgramConfig program_config = ConfigHelper.load ();
        var app = new Demo (program_config);

        app.start ();
        app.activate ();
        app.redraw ();
        app.run ();
        app.stop ();

        Pid child_pid;

        string launch_program_command = program_config.is_editor_launch_command_set () ? 
            program_config.editor_launch_command : program_config.terminal_launch_command;

        string[] process_spawn_args = "%s %s".printf (launch_program_command, (string) app.selected_directory).split (" ");

        try {
            GLib.Process.spawn_async (
                null,
                process_spawn_args,
                Environ.get (),
                SpawnFlags.SEARCH_PATH | SpawnFlags.DO_NOT_REAP_CHILD,
                null,
                out child_pid
            );
        } catch (SpawnError e) {
            error (e.message);
        }
            return EXIT_SUCCESS;
    }
}
