namespace Bro.Structs {
    public struct ProgramConfig {
        string projects_path;
        string terminal_launch_command;
        string editor_launch_command;

    public bool is_editor_launch_command_set () {
            return editor_launch_command != "";
        }
    }
}
