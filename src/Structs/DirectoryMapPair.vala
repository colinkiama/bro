namespace Bro.Structs {
    public struct DirectoryMapPair {
        public string name;
        public string path;
    }
}
