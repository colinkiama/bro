namespace Bro.Model {
    public class Result<T> : GLib.Object {
        private T data;
        private Error error;

        public Result.with_data (T data) {
            this.data = data;
        }

        public Result.with_error (Error error) {
            this.error = error;
        }

        public T reveal () {
            return data;
        }

        public Error expose_error () {
            return error;
        }

        public bool has_error () {
            return error != null;
        }
    }
}
