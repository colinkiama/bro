using Bro.Model;
using Gee;

namespace Bro.Helpers {
    public class FileHelper {
        private FileHelper () {

        }

        public static Result<ArrayList<File>> list_directories_in_directory (File directory,
            GLib.Cancellable? cancellable = null) {
            FileType file_type = directory.query_file_type (FileQueryInfoFlags.NONE, cancellable);

            if (file_type != FileType.DIRECTORY) {
                return new Result<ArrayList<File>>.with_error (new Error (
                    FileError.quark (),
                    FileError.NOTDIR,
                    "Not a directory"
                ));
            } else {
                try {
                    ArrayList<File> files = obtain_files (directory, cancellable);
                    return new Result<ArrayList<File>>.with_data (files);
                } catch (Error e) {
                    return new Result<ArrayList<File>>.with_error (e);
                }
            }
        }

        private static ArrayList<File> obtain_files (File directory, GLib.Cancellable? cancellable) throws GLib.Error {
            ArrayList<File> files_to_obtain = new ArrayList<File> ();
            GLib.FileEnumerator enumerator = directory.enumerate_children (GLib.FileAttribute.STANDARD_NAME,
                GLib.FileQueryInfoFlags.NONE, cancellable
            );

            string directory_path = directory.get_path ();
            for (GLib.FileInfo? info = enumerator.next_file ();
                info != null; info = enumerator.next_file (cancellable)
            ) {
                File file = File.new_for_path (GLib.Path.build_filename (directory_path, info.get_name ()));
                if (file.query_file_type (FileQueryInfoFlags.NONE, cancellable) != FileType.DIRECTORY) {
                    continue;
                }

                files_to_obtain.add (file);
            }

            return files_to_obtain;
        }
    }
}
