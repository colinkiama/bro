using Bro.Model;
using Bro.Structs;

namespace Bro.Helpers {
    public class ConfigHelper {
        public const string BRO_EDITOR_VARIABLE = "BRO_EDITOR";
        public const string BRO_TERMINAL_VARIABLE = "BRO_TERMINAL";
        public const string BRO_PROJECTS_DIR_VARIABLE = "BRO_PROJECTS_DIR";

        // TODO: Set these values by generating a constants with Meson.
        public const string TERMINAL_LAUNCH_COMMAND_FALLBACK = "io.elementary.terminal -t -w";
        public static string projects_dir_fallback = GLib.Environment.get_variable ("HOME");

        private ConfigHelper () {}

        public static ProgramConfig load () {
            // TODO: Set these values by generating a constants with Meson.
            string projects_dir_fallback = GLib.Environment.get_variable ("HOME");

            return ProgramConfig () {
                projects_path = load_from_environment_variable (BRO_PROJECTS_DIR_VARIABLE, projects_dir_fallback),
                terminal_launch_command = load_from_environment_variable (
                    BRO_TERMINAL_VARIABLE,
                    TERMINAL_LAUNCH_COMMAND_FALLBACK
                ),
                editor_launch_command = load_from_environment_variable (BRO_EDITOR_VARIABLE, "")
            };
        }

        public static string load_from_environment_variable (string variable_to_load, string fallback_value) {
           string loaded_variable = GLib.Environment.get_variable (variable_to_load);
           if (loaded_variable != null && loaded_variable.strip () != "") {
               return loaded_variable;
           }

           return fallback_value;
       }
   }
}
